#!/usr/bin/env bash


docker stack rm lab
echo 'y' | docker system prune

sleep 15
docker stack deploy -c docker-compose-dev.yml lab

sleep 15
echo 'y' | docker system prune

echo 'All systems up!'