from logger.logger import setup_logging
from common import config_parser
import logging
import os
import redis
from time import sleep
import pika
import json


def find_members_within_radius(lat, lng, radius, looking_for):
    """
    Return the members of a sorted set populated with geospatial information using GEOADD,
    (In this case defined by the key == loooking_for) which are within the borders of the area specified
    with the center location and the maximum distance from the center (the radius).

    :param lat:
    :param lng:
    :param radius:
    :param looking_for: the name of the key wich we'll be looking for "drivers", "riders"
    :return:
    """
    ans = rs.georadius(name=looking_for,
                       longitude=lng,
                       latitude=lat,
                       unit="m",
                       radius=radius,
                       withcoord=True,
                       withdist=True)
    return ans


def send_to_client(ch, msg):
    body = json.dumps(msg)

    # Declare the queue
    ch.queue_declare(queue="send_to_clients", exclusive=False)

    # Send a message
    if ch.basic_publish(exchange='messages-with-clients',
                        routing_key='to_clients_queue',
                        body=body,
                        properties=pika.BasicProperties(content_type='application/json',
                                                        delivery_mode=1)):
        log.info('Message publish was confirmed')
    else:
        log.info('Message could not be confirmed')


def status_process(ch, method, properties, body):
    while True:
        try:
            rs = redis.StrictRedis(port=REDIS_PORT, host=REDIS_HOST)
            break
        except:
            log.warning('Something happen with Redis, check it out. Retrying to connect in {} seconds'.format(1))
            sleep(1)
            continue

    msg = json.loads(body)
    timestamp = msg['timestamp']
    socket_id = msg['socketId']

    # Acting on connection events
    if 'toLog' in msg:
        if msg['toLog'] == 'join':
            log.info('LOGIN client {} at socket "{}"'.format(msg['actorUuid'], socket_id))
            # Add secondary index to keep track of socket to uuid relationship
            rs.hset('socket_to_uuid', socket_id, msg['actorUuid'])

            send_to_client(ch, {'body': 'welcome!', 'socket_id': socket_id})

        elif msg['toLog'] == 'leave':
            uuid_to_update = rs.hget('socket_to_uuid', socket_id)
            log.info('LOGOUT client {} at socket "{}"'.format(uuid_to_update, socket_id))

            if rs.exists(uuid_to_update) == 1:
                rs.delete(uuid_to_update)
                log.info('delete master key, done')
            else:
                log.warning('master key not found')

            if type(rs.zrank('driver', uuid_to_update)) == int:
                rs.zrem('driver', uuid_to_update)
                log.info('delete geolocation, done')
            else:
                log.warning('geolocation key not found')

            if rs.hexists('socket_to_uuid', socket_id) == 1:
                rs.hdel('socket_to_uuid', socket_id)
                log.info('delete secondary index, done')
            else:
                log.warning('UUID not found in secondary index')

    #  Processing jobs
    else:
        actor_uuid = msg['actorUuid']
        # Processing status events

        if msg['job'] == 'location-update':
            log.info('location update for client "{}"'.format(actor_uuid))
            rs.geoadd(msg['actorType'], msg['lng'], msg['lat'], actor_uuid)
            rs.hset(actor_uuid, 'timestamp', timestamp)
            send_to_client(ch, {'body': 'welcome!', 'socket_id': socket_id})

        elif msg['job'] == 'status-update':
            log.info('status update for client "{}"'.format(actor_uuid))
            rs.hmset(actor_uuid, msg)

        else:
            log.warning('Processing job that does not exist')


#############################################################################################
# Rabbit MQ consumer SetUp - Routing
##############################################################################################

# f = '%(asctime)s %(levelname)s %(name)s %(message)s'
# logging.basicConfig(level=logging.INFO, format=f)


setup_logging()
log = logging.getLogger(__name__)


try:
    log.info('Reading config file')
    config = config_parser('/app/config.yml')
    log.info('Config file successfully loaded')
except:
    log.error('Can not read config.yml', exc_info=True)
    raise SystemError

log.info('Setting configuration variables')


REDIS_HOST = config['connections']['redis-location']['host']
REDIS_PORT = config['connections']['redis-location']['port']
DATABUS_HOST = config['connections']['databus']['host']
DATABUS_PORT = config['connections']['databus']['port']
DATABUS_VHOST = config['connections']['databus']['vhost']
DATABUS_USER = config['connections']['databus']['user']
DATABUS_PASS = config['connections']['databus']['pass']

DELAY = config['execution']['delay']

EX_NAME = config['msg-config']['exchange']['name']
EX_TYPE = config['msg-config']['exchange']['type']
EX_DURABLE = config['msg-config']['exchange']['durable']

Q_LISTEN = config['msg-config']['queue']['listening_queue']
Q_EXCLUSIVE = config['msg-config']['queue']['exclusive']
Q_CLIENTS = config['msg-config']['queue']['frw_to_clients_queue']

CONSUME_NOACK = config['msg-config']['consume']['no_ack']


log.info('Done')





while True:
    try:
        log.info('connecting to host {}:{} as {}'.format(DATABUS_HOST, DATABUS_PORT, DATABUS_USER))
        credentials = pika.PlainCredentials(DATABUS_USER, DATABUS_PASS)
        parameters = pika.ConnectionParameters(DATABUS_HOST, DATABUS_PORT, DATABUS_VHOST, credentials)
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.basic_qos(prefetch_count=1)

        channel.exchange_declare(exchange='messages-with-clients',
                                 exchange_type='direct',
                                 durable=True
                                 )
        result = channel.queue_declare(queue='status-update', exclusive=True)

        channel.queue_bind(exchange='messages-with-clients',
                           queue='status-update')

        channel.basic_consume(status_process,
                              queue='status-update',
                              no_ack=True)

        log.info('connected to {}'.format(DATABUS_HOST))

        try:
            log.info('ready to consume. Send me something!')
            channel.start_consuming()
            log.info('connected to {}'.format(REDIS_HOST))

        # recover form all events
        except Exception:
            log.warning('something happen. Recovering in {} seconds. Some data could have been lost.'.format(DELAY))
            sleep(DELAY)
            connection.close()
            continue
    # recover from all events
    except Exception:
        log.info('Could not connect to {}. Waiting {} seconds and retrying.'.format(DATABUS_HOST, DELAY))
        sleep(DELAY)
        continue
