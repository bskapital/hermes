import json
import redis
import logging
import os
from time import sleep
from common import config_parser

CONFIG_FILE = os.environ.get('CONFIG_FILE')
LOGGER = logging.getLogger(__name__)


def redis_location():
    conf = config_parser(CONFIG_FILE)
    while True:
        try:
            rs = redis.StrictRedis(port=conf['connections']['redis-location']['port'],
                                   host=conf['connections']['redis-location']['host'])
            return rs
        except:
            LOGGER.warning('Something happen with Redis, check it out. Retrying to connect in {} seconds'.format(5))
            sleep(5)
            continue


def status_process(ch, method, properties, body):

    rs = redis_location()

    msg = json.loads(body)
    timestamp = msg['timestamp']
    socket_id = msg['socketId']

    # Acting on connection events
    if 'toLog' in msg:
        if msg['toLog'] == 'join':
            LOGGER.info('LOGIN client {} at socket "{}"'.format(msg['actorUuid'], socket_id))
            # Add secondary index to keep track of socket to uuid relationship
            rs.hset('socket_to_uuid', socket_id, msg['actorUuid'])

            # send_to_client(ch, {'body': 'welcome!', 'socket_id': socket_id})

        elif msg['toLog'] == 'leave':
            uuid_to_update = rs.hget('socket_to_uuid', socket_id)

            if uuid_to_update is not None:

                LOGGER.info('LOGOUT client {} at socket "{}"'.format(uuid_to_update, socket_id))

                if rs.exists(uuid_to_update) == 1:
                    rs.delete(uuid_to_update)
                    LOGGER.info('delete master key, done')
                else:
                    LOGGER.warning('master key not found')

                if type(rs.zrank('driver', uuid_to_update)) == int:
                    rs.zrem('driver', uuid_to_update)
                    LOGGER.info('delete geolocation, done')
                elif type(rs.zrank('rider', uuid_to_update)) == int:
                    rs.zrem('driver', uuid_to_update)
                    LOGGER.info('delete geolocation, done')
                else:
                    LOGGER.warning('geolocation key not found')

                if rs.hexists('socket_to_uuid', socket_id) == 1:
                    rs.hdel('socket_to_uuid', socket_id)
                    LOGGER.info('delete secondary index, done')
                else:
                    LOGGER.warning('UUID not found in secondary index')

    #  Processing jobs
    else:
        actor_uuid = msg['actorUuid']
        # Processing status events

        if msg['job'] == 'location-update':
            LOGGER.info('location update for client "{}"'.format(actor_uuid))
            rs.geoadd(msg['actorType'], msg['lng'], msg['lat'], actor_uuid)
            rs.hset(actor_uuid, 'timestamp', timestamp)
            # send_to_client(ch, {'body': 'welcome!', 'socket_id': socket_id})

        elif msg['job'] == 'status-update':
            LOGGER.info('status update for client "{}"'.format(actor_uuid))
            rs.hmset(actor_uuid, msg)

        else:
            LOGGER.critical('Processing job that does not exist')

