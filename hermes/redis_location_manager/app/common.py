import logging
import logging.config
from logging import Formatter
import pathlib
import yaml
import time
import os

LOG_CONFIG = os.environ.get('LOG_CONFIG')


def config_parser(config_file):
    """read the default config file in this container
    """

    path = pathlib.Path(config_file)
    try:
        with open(path, 'rt') as my_file:
            c = yaml.safe_load(my_file.read())
        return c
    except:
        raise SystemError


def setup_logging():
    """Setup logging configuration
    https://docs.python.org/3/howto/logging.html
    https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
    :return:
    """
    path = pathlib.Path(LOG_CONFIG)
    try:
        with open(path, 'rt') as my_file:
            config = yaml.safe_load(my_file.read())
        logging.config.dictConfig(config)
    except:
        raise SystemError


class UTCFormatter(Formatter):
    converter = time.gmtime
