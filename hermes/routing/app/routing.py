
import pika
import requests
import json
import os
from time import sleep

GOOGLE_MAPS_API_KEY = os.environ.get('GOOGLE_MAPS_API_KEY')
GOOGLE_DIRECTIONS_URL = os.environ.get('GOOGLE_DIRECTIONS_URL')

DATABUS_HOST = os.environ.get('DATABUS_HOST')
DATABUS_PORT = os.environ.get('DATABUS_PORT')
DATABUS_VHOST = os.environ.get('DATABUS_VHOST')
DATABUS_USER = os.environ.get('DATABUS_USER')
DATABUS_PASS = os.environ.get('DATABUS_PASS')

TRAFFIC_MODEL = os.environ.get('TRAFFIC_MODEL')
LANGUAGE = os.environ.get('LANGUAGE')
REGION = os.environ.get('REGION')


def google_directions(origin_lat, origin_lon, dest_lat, dest_lon, departure_time, language):
    """Directions from google maps

    Documented here: https://developers.google.com/maps/documentation/directions/client-library
    :param origin_lat: str
    :param origin_lon: str
    :param dest_lat:str
    :param dest_lon: str
    :param departure_time:
    :return:
    """

    params = {'key': GOOGLE_MAPS_API_KEY,
              'origin': origin_lat + ',' + origin_lon,
              'destination': dest_lat + ',' + dest_lon,
              'mode': 'driving',
              'units': 'metric',
              'departure_time': departure_time,
              'language': language,
              'traffic_model': TRAFFIC_MODEL,
              'region': REGION}

    r = requests.get(url=GOOGLE_DIRECTIONS_URL, params=params)

    if r.status_code == 200:
        ans = r.json()
        return ans
    else:
        # TODO: implement error handling
        return {'status_code': r.status_code}


# def osrm_self_hosted(origin_lat, origin_lon, dest_lat, dest_lon):
#     coordinates = str(origin_lon) + ',' + str(origin_lat) + ';' + str(dest_lon) + ',' + str(dest_lat)
#     endpoint = '127.0.0.1:9966/route/v1/car/'
#
#     url = endpoint + coordinates
#
#     params = {'alternatives': 'false',
#               'steps': 'false',
#               'geometries': 'false'
#               }
#
#     r = requests.get(url=url, params=params)
#     ans = r.json()
#     return ans


def routing(json_data, provider='google'):
    """" Query the chosen provider for routing instructions

    """
    data = json.loads(json_data)

    origin_lat = str(data['locations']['start']['lat'])
    origin_lon = str(data['locations']['start']['lng'])
    dest_lat = str(data['locations']['end']['lat'])
    dest_lon = str(data['locations']['end']['lng'])
    departure_time = data['departure_time']
    language = data['language']

    if provider == 'google':
        response = google_directions(origin_lat=origin_lat,
                                     origin_lon=origin_lon,
                                     dest_lat=dest_lat,
                                     dest_lon=dest_lon,
                                     departure_time=departure_time,
                                     language=language)

        return json.dumps(response)
    else:
        raise NotImplementedError


def on_request(ch, method, props, body):

    response = routing(body)

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id=props.correlation_id),
                     body=response)
    ch.basic_ack(delivery_tag=method.delivery_tag)


sleep(15)
credentials = pika.PlainCredentials(DATABUS_USER, DATABUS_PASS)
parameters = pika.ConnectionParameters(DATABUS_HOST, DATABUS_PORT, DATABUS_VHOST, credentials)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()
channel.queue_declare(queue='routing_queue', exclusive=False, durable=True, arguments={"message-ttl": 180000})
channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue='routing_queue')

print(" [x] Awaiting Routing_RPC requests")
channel.start_consuming()
