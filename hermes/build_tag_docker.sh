#!/usr/bin/env bash


set -ex

DIR="${1}"

REGISTRY="mrjavi"
REPO="hermes"
NAME=${REPO}"_"${DIR}


docker build -t ${REGISTRY}/${REPO}:${DIR} -f ./${DIR}/Dockerfile ${DIR}/
#docker image push ${REGISTRY}/${REPO}:${DIR}



