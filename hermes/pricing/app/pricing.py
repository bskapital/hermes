# -*- coding: utf-8 -*-
""" Pricing service

This module handle the relationship with the clients in respect of the pricing of a trip.



Todo:
    * multi-stop trips
    * Recalculate with change of destination

"""
import json
import math
import os
import pika
from rabbimq import RpcClient
from time import sleep
import uuid
from pprint import pprint
import redis

# Env var must be quoted in "" they bring a string back
# Need to parse to a numerical value  https://stackoverflow.com/q/485789/3512107
MTR_VALUE = float(os.environ.get('PRICING_METER_VALUE'))
MIN_VALUE = float(os.environ.get('PRICING_MINUTE_VALUE'))
MINIMUM = float(os.environ.get('PRICING_MINIMUM_FEE'))
SERVICE_FEE = float(os.environ.get('PRICING_SERVICE_FEE'))

DATABUS_HOST = os.environ.get('DATABUS_HOST')
DATABUS_PORT = os.environ.get('DATABUS_PORT')
DATABUS_VHOST = os.environ.get('DATABUS_VHOST')
DATABUS_USER = os.environ.get('DATABUS_USER')
DATABUS_PASS = os.environ.get('DATABUS_PASS')


def send_to_dispacher(data):
    pprint(data)


def get_routing(json_trip_info):
    """RPC for routing service

    :param json_trip_info:
    :return:
    """
    # Connect to the "routing_queue" at RabbitMQ service
    routing_rpc = RpcClient(queue='routing_queue')
    # Send RPC call
    json_response = routing_rpc.call(json_trip_info)
    # Decode JSON response
    response = json.loads(json_response)

    return response


def trip_quote(json_data):
    """ Given origin and end locations returns a fare for a trip
    :param json_data: json containing trip info received from client
    :return:  json with dictionary
    """

    # Parse the json data send by the client
    data = json.loads(json_data)

    # To get routing we only need start / end locations and departure time
    # extract it from the incoming data and serialize before sending to the routing
    # service
    data4routing = json.dumps({'locations': data['locations'],
                               "departure_time": data['departure_time'],
                               "language": data['language']})

    # Send request to the routing service
    routing = get_routing(data4routing)

    # Extract basic info to compute pricing and send back to the client
    distance = routing['routes'][0]['legs'][0]['distance']['value']
    seconds_in_transit = routing['routes'][0]['legs'][0]['duration_in_traffic']['value']
    minutes_in_transit = math.ceil(seconds_in_transit / 60)

    #############################################################################################
    # Unique identifier for this trip
    #############################################################################################
    trip_uuid = uuid.uuid4()

    #############################################################################################
    # Compute pricing
    #############################################################################################
    # TODO: Pricing formula revision
    #       with surge computation
    base = float(max(min((distance / 100) * MTR_VALUE, minutes_in_transit * MIN_VALUE), MINIMUM))
    surge = 0.0

    #############################################################################################
    # Send all the data to the dispacher who will be waiting for client response
    #############################################################################################
    send_to_dispacher({'trip_uuid_int': trip_uuid.int,
                       "trip_routing": routing})

    #############################################################################################
    # Make structure for client
    #############################################################################################
    ans = {'trip_uuid_int': trip_uuid.int,
           'base': base,
           'surge': surge,
           'service': SERVICE_FEE / 100,
           'travelTime': minutes_in_transit,
           'distance': distance}

    # Return to client
    return json.dumps(ans)


def on_request(ch, method, props, body):
    """

    :param ch:
    :param method:
    :param props:
    :param body:
    :return:
    """
    response = trip_quote(body)

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id=props.correlation_id),
                     body=response)
    ch.basic_ack(delivery_tag=method.delivery_tag)


sleep(30)




##############################################################################################
# Rabbit MQ consumer SetUp
##############################################################################################

#
# credentials = pika.PlainCredentials(DATABUS_USER, DATABUS_PASS)
# parameters = pika.ConnectionParameters(DATABUS_HOST, DATABUS_PORT, DATABUS_VHOST, credentials)
# connection = pika.BlockingConnection(parameters)
#
# channel = connection.channel()
# channel.queue_declare(queue='pricing_queue', exclusive=False, durable=True, arguments={"message-ttl": 180000})
# channel.basic_qos(prefetch_count=1)
# channel.basic_consume(on_request, queue='pricing_queue')
#
# # print(" [x] Awaiting Pricing_RPC requests")
# channel.start_consuming()
