import pika
import uuid
import os

DATABUS_HOST = os.environ.get('DATABUS_HOST')
DATABUS_PORT = os.environ.get('DATABUS_PORT')
DATABUS_VHOST = os.environ.get('DATABUS_VHOST')
DATABUS_USER = os.environ.get('DATABUS_USER')
DATABUS_PASS = os.environ.get('DATABUS_PASS')


class RpcClient(object):
    """ Remote Procedure Call thought RabbitMQ with direct reply-to

    Based on: https://www.rabbitmq.com/tutorials/tutorial-six-python.html
    Modify because: https://www.rabbitmq.com/direct-reply-to.html

    RPC (request/reply) is a popular pattern to implement with a messaging broker like RabbitMQ.
    The typical way to do this is for RPC clients to send requests that are routed to a long lived (known)
    server queue. The RPC server(s) consume requests from this queue and then send replies to each client
    using the queue named by the client in the reply-to header.

    Where does the client's queue come from? The client can declare a single-use queue for each
    request-response pair. But this is inefficient; even a transient unmirrored queue can be expensive
    to create and then delete (compared with the cost of sending a message). This is especially true
    in a cluster as all cluster nodes need to agree that the queue has been created, even if it is unmirrored.

    So alternatively the client can create a long-lived queue for its replies. But this can be fiddly to manage,
    especially if the client itself is not long-lived.

    The direct reply-to feature allows RPC clients to receive replies directly from their RPC server,
    without going through a reply queue. ("Directly" here still means going through the same connection
    and a RabbitMQ node; there is no direct network connection between RPC client and RPC server processes.)

    ...

    Reply messages sent using this mechanism are in general not fault-tolerant; they will be discarded
    if the client that published the original request subsequently disconnects.
     The assumption is that an RPC client will reconnect and submit another request in this case.
    """

    def __init__(self, queue):

        self.credentials = pika.PlainCredentials(DATABUS_USER, DATABUS_PASS)
        self.parameters = pika.ConnectionParameters(DATABUS_HOST, DATABUS_PORT, DATABUS_VHOST, self.credentials)
        self.connection = pika.BlockingConnection(self.parameters)
        self.queue = queue
        self.channel = self.connection.channel()

        self.channel.basic_consume(self.on_response,
                                   no_ack=True,
                                   queue='amq.rabbitmq.reply-to')

        self.response = None
        self.corr_id = None

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, n):
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='',
                                   routing_key=self.queue,
                                   properties=pika.BasicProperties(reply_to='amq.rabbitmq.reply-to',
                                                                   correlation_id=self.corr_id,
                                                                   content_type='application/json'),
                                   body=n)
        while self.response is None:
            self.connection.process_data_events()
        return self.response
