#PRICING ENGINE 

Here we find a price for that trip.


#### How is the process
1. Engine and data bus "rabbit" is up and running
2. A client request a quotation. To process the request the engine needs:

   * Initial location 
   * End location
   * Any intermediate stops
   
     * First tuple = Origin
     * Last tuple = Destination
     * Any tuples in between are considered stops
     * The locations mus be provided as a list of tuples  [(), ()]. Each tuple must contain 
     latitude and longitude in decimal form with minimum six decimal. For example (4.713303,-74.0698242)
         
         - Must be reviewed: There is a issue with tuples when serialized to json. There is not notion of tuples in json.  
   
   * Time to start the trip
   * Pricing variables defined and set as environmental variables in the machine running the engine. 
   At the moment we use:
   
     - 100 meters price set at "PRICING_METER_VALUE"
     - 60 seconds price set at "PRICING_MINUTE_VALUE"
     - The minimum value a ride can have set at "PRICING_MINIMUM_FEE"
     - Service fee if any set at "PRICING_SERVICE_FEE"
   
        * Monetary variables ar given in cents to avoid dealing with decimal formats.
        * Time variables are given in seconds. 
        
3. The engine parse the information and request to a maps provider the  suggested route, estimate distance 
and time in transit for that trip.

4. The engine process using internal formula to obtain a quote, that could include a surge in price given the 
location and time.

5. The quote is post back to the data bus for client consumption.

#### Maps providers
1. Implemented
    * Google maps. It needs api key value set at "GOOGLE_MAPS_API_KEY"
    * Self hosted OSRM - Works but there is no traffic information and in cities with heavy traffic the time
    differences are a  big issue. 
2. For implementation and evaluation:
    * Mapbox
    * Mapquest
    * Here
    
### Some links that help me
* [Why did Uber switch back to Google Map from MapBox?](https://www.quora.com/Why-did-Uber-switch-back-to-Google-Map-from-MapBox)
* [Google Maps API pricing got you down? See these awesome alternatives](http://geoawesomeness.com/google-maps-api-alternatives-best-cheap-affordable/)
* [5 things to know before choosing digital maps API](https://jungleworks.com/google-vs-mapbox/)
* [A Guide to Choose Maps API for your On-Demand App](https://jungleworks.com/maps-api-mobile-app-guide-google-mapbox-openstreet-pricing/)



