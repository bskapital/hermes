"""
Mock of a client_rider_desktop

All this must be implemented for IOs and Android

"""
import json
from datetime import datetime


from flask import request
from time import sleep
from rabbimq import RpcClient


def my_converter(o):
    """
    serialize  datetime

    It is easy to serialize a Python data structure as JSON, we just need to call
    the json.dumps method, but if our data stucture contains a datetime object
    we'll get an exception: TypeError: datetime.datetime(...) is not JSON serializable

    The json.dumps method can accept an optional parameter called default which is
    expected to be a function. Every time JSON tries to convert a value it does not
    know how to convert it will call the function we passed to it. The function
    will receive the object in question, and it is expected to return the JSON
    representation of the object.

    In the function we just call the __str__ method of the datetime object that
    will return a string representation of the value. This is what we return.

    https://code-maven.com/serialize-datetime-object-as-json-in-python

    :param o:
    :return: str
    """
    if isinstance(o, datetime):
        return o.__str__()


def quote_request(data):
    # print("********************quote request gets ************************************")
    # print(data)
    # print("********************asking RPC with: ************************************")
    pricing_rpc = RpcClient(queue='pricing_queue')
    my_request = pricing_rpc.call(data)
    response = json.loads(my_request)
    return response
