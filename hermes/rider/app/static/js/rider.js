var socket = io.connect('http://socket.app.test', { 'forceNew': true });
var isDev = true;
var actorUpdateFrequency = 2500;
var randomDist = "200";
var decimalDecimals = 6;
var numberFormatCountry = 'en-US'
var clientCurrency = 'USD'
var actorUuid;
var mapTilesServer = 'http://maps.app.test/styles/osm-bright/{z}/{x}/{y}.png'
//var mapTilesServer = 'http://mapstiles:8081/styles/osm-bright/{z}/{x}/{y}.png'
var currentLocation = L.latLng(4.6668663,-74.0548737)   // C.C. Andino Bogota CO   To use when live location is unavailable
var endMarkerLoc = [4.6623542,-74.0634867]   // 93 Park Bogota CO
var zoomDefault = 18;
var markersPos = [];
var map;
var startMarker;
var stopMarker;
var endMarker;
var riderLocation;
var geoOptions = {
   timeout: 30000,
   enableHighAccuracy: true,
   maximumAge: 0
};
var actorGeo = {};
var actorData = {};
var tripData= {};


// ###############################################################################
// Main function
// ###############################################################################
//function initMap(currentLocation) {
function initMap() {
  // create map with Leaflet center at rider current location
  var mapOptions = {
    center: currentLocation,
    zoom: zoomDefault,
    dragging: true,
    trackResize: true,
  }

  map = new L.map('map', mapOptions);

  // Creating a map layer object
  var layer = new L.TileLayer(mapTilesServer);
  map.addLayer(layer);

  // START MARKER
  var startIcon = new L.icon({
    iconUrl: '../static/assets/green-pin.png',
    iconSize: [50, 50],
  });
  startMarker = new L.marker(currentLocation, {
    icon: startIcon,
    draggable: true,
    alt: 'Start'
  }).addTo(map);


  // END MARKER
  // create a random destination
  endMarkerLoc = randomGeo(currentLocation, randomDist);
  var endIcon = new L.icon({
    iconUrl: '../static/assets/red-pin.png',
    iconSize: [50, 50],
  });
  endMarker = new L.marker(endMarkerLoc, {
    icon: endIcon,
    draggable: true,
    alt: 'End'
  }).addTo(map);


  // Create some id
  actorUuid = uuid4();


  // basic data structures
  actorGeo.actorType='rider';
  actorGeo.job='location-update';
  actorGeo.actorUuid=actorUuid;
  actorGeo.lat=startMarker.getLatLng().lat;
  actorGeo.lng=startMarker.getLatLng().lng;


  actorData.actorUuid=actorUuid;
  actorData.actorType='rider';
  actorData.job='status-update';
  actorData.deviceStatus='on';;
  actorData.driverStatus='off'
  actorData.tripStatus='off';


  tripData.startLat =startMarker.getLatLng().lat;
  tripData.startLng =startMarker.getLatLng().lng;
  tripData.stopLat = ""
  tripData.stopLng = ""
  tripData.endLat = endMarker.getLatLng().lat;
  tripData.endLng = endMarker.getLatLng().lng;



  // Logging initial connection
  myEmit('connection-update');
  myEmit('status-update');





  // Fit zoom to bounds
  markersPos = [startMarker.getLatLng(), endMarker.getLatLng()];
//  map.panTo(driverMarker.getLatLng())
  fitBounds();



  // Update onscreen initial information
  updateMarkerPosition(startMarker.getLatLng(), 'start');
  updateMarkerPosition(endMarker.getLatLng(), 'end');

  // LISTENERS
  startMarker.on('drag', function(e) {
    updateMarkerPosition(startMarker.getLatLng(), 'start');
  });

  endMarker.on('drag', function(e) {
    updateMarkerPosition(endMarker.getLatLng(), 'end');
  });


  startMarker.on('dragend', function(e) {
    markersPos = [startMarker.getLatLng(), endMarker.getLatLng()];
    updateLocations(startMarker.getLatLng(), endMarker.getLatLng());
    updateActorLocation(startMarker.getLatLng(), 'rider');
    broadcastLocation('rider');
    fitBounds();
  });

  endMarker.on('dragend', function(e) {
    markersPos = [startMarker.getLatLng(), endMarker.getLatLng()];
    updateLocations(startMarker.getLatLng(), endMarker.getLatLng());
    fitBounds();
  });


  // Update data to server every n seconds
  // Location and status data are updated.
  setInterval(function(){
    actorGeo.lat=startMarker.getLatLng().lat;
    actorGeo.lng=startMarker.getLatLng().lng;
    actorGeo.timestamp=Date.now();
    myEmit('location-update');
      }, actorUpdateFrequency);



}



// ############################################################################
// Update data set for socket transmission
// ############################################################################

// Emit with confirmation
function myEmit(emitType) {

  if (emitType == 'connection-update') {
    queue = 'status-update';
    body = dataPack(queue, {actorUuid: actorUuid, toLog:'join', timestamp: Date.now()});

  } else if (emitType == 'status-update') {
    queue = 'status-update';
    actorData.timestamp=Date.now();
    body = dataPack(queue, actorData);

  } else if (emitType == 'location-update'){
    queue = 'status-update';
    actorGeo.timestamp=Date.now();
    body = dataPack(queue, actorGeo);

  } else if (emitType == 'price-request'){
    queue = 'price-request';
    tripData.timestamp=Date.now();
    body = dataPack(queue, tripData);
  };




  socket.emit('frw_to_service', body, function(response) {
    console.log(response);
  });

}



// prepare the data with info to be used for constructing a RabbitMQ message
function dataPack(queue, body) {
  x ={
    queue: queue,
    body: body
    };
  return JSON.stringify(x)
}


function updateLocations(startMarkerPos, endMarkerPos) {

  locations = {
    "start": {
      "lat": startMarkerPos.lat,
      "lng": startMarkerPos.lng
    },
    "end": {
      "lat": endMarkerPos.lat,
      "lng": endMarkerPos.lng
    }
  }
}



// ############################################################################
// Ask for Quote - AJAX
// ############################################################################
$(function() {
  $('#sendBtn').click(function() {

    tripData.startLat = startMarker.getLatLng().lat;
    tripData.startLng = startMarker.getLatLng().lng;
    tripData.stopLat = ""
    tripData.stopLng = ""
    tripData.endLat = endMarker.getLatLng().lat;
    tripData.endLng = endMarker.getLatLng().lng;
    tripData.interCities = false;
    tripData.passengers = 0;
    tripData.bags = 0
    tripData.departureTime = 'now';
    tripData.pets = false;

    myEmit('price-request')

  });
});


function post_quote_to_client(response) {

  var price = response['base'] + response['surge'] + response['service']
  var travelTime = response['travelTime'];
  var travelDistance = response['distance']

  document.getElementById('rate').innerHTML = formatCurrency(price / 100, decimalDecimals);
  document.getElementById('waitTime').innerHTML = 5 + ' mins';
  document.getElementById('travelTime').innerHTML = travelTime + ' mins';
  document.getElementById('travelDistance').innerHTML = formatDecimal(travelDistance / 1000, 1) + ' kms';

}



function updateActorLocation(MarkerPos, actor){
    var data = {
            lat: MarkerPos.lat,
            lng: MarkerPos.lng,
            timestamp: Date.now()
        }

    if (actor=='driver'){
       data.uuid = driverUuid;
       data.actorType = 'driver';
       driverLocation = data;
    } else if (actor=='rider') {
       data.uuid = riderUuid;
       data.actorType = 'rider';
       riderLocation = data;
    }
}





// ############################################################################
// Get current position from device
//https://www.tutorialspoint.com/html5/geolocation_getcurrentposition.htm
// ############################################################################
function returnLocation(position) {
  var loc = L.latLng(position.coords.latitude, position.coords.longitude);
  initMap(loc);
}

function errorHandler(err) {
  if (err.code == 1) {
    alert("Error: Access is denied!");
  } else if (err.code == 2) {
    alert("Error: Position is unavailable!");
  }
}

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(returnLocation, errorHandler, geoOptions);
  } else {
    alert("Sorry, browser does not support geolocation!");
  }
}



// ##########################################################################
// changes the position of the marker
// ##########################################################################

function updateMarkerPosition(latLng, loc) {
  if (loc == 'start') {
    document.getElementById('startLat').innerHTML = formatDecimal(latLng.lat, decimalDecimals);
    document.getElementById('startLng').innerHTML = formatDecimal(latLng.lng, decimalDecimals);
  } else if (loc == 'end') {
    document.getElementById('endLat').innerHTML = formatDecimal(latLng.lat, decimalDecimals);
    document.getElementById('endLng').innerHTML = formatDecimal(latLng.lng, decimalDecimals);
  } else if (loc == 'driver') {
    document.getElementById('driverLat').innerHTML =formatDecimal(latLng.lat, decimalDecimals);
    document.getElementById('driverLng').innerHTML =  formatDecimal(latLng.lng, decimalDecimals);
  }
}

// ##########################################################################
// Formatting
// ##########################################################################
function formatDecimal(num, dec){
    //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed
    return parseFloat(num).toFixed(dec);
}

function formatCurrency(num, dec){
    // Create our number formatter.   https://stackoverflow.com/a/16233919
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat
    var formatter = new Intl.NumberFormat(numberFormatCountry, {
      style: 'currency',
      currency: clientCurrency,
    });
    return formatter.format(num, 0)
}

function fitBounds(){
  // Fit to Bounds
  var bounds = new L.LatLngBounds(markersPos);
  map.fitBounds(bounds, {
     padding: [50, 50]
   });


}

// ##########################################################################
// Some functions
// ##########################################################################
//This is for development only. In real life the UUID must be creater at the
// server and be unique.
//https://stackoverflow.com/a/2117523
function uuid4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )
}



//Create random lat/long coordinates in a specified radius around a center point
// https://stackoverflow.com/a/31280435
function randomGeo(center, radius) {
    var y0 = center.lat;
    var x0 = center.lng;
    var rd = radius / 111300; //about 111300 meters in one degree

    var u = Math.random();
    var v = Math.random();

    var w = rd * Math.sqrt(u);
    var t = 2 * Math.PI * v;
    var x = w * Math.cos(t);
    var y = w * Math.sin(t);

    //Adjust the x-coordinate for the shrinking of the east-west distances
    var xp = x / Math.cos(y0);

    var newlat = y + y0;
    var newlon = x + x0;
    var newlon2 = xp + x0;

    return {
        'lat': newlat.toFixed(5),
        'lng_no_adj': newlon.toFixed(5),
        'lng': newlon2.toFixed(5),
    };
}
