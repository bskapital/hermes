from flask import Flask, render_template, request, jsonify
from price_request import quote_request, my_converter
import json


app = Flask(__name__)


@app.route("/")
def main():
    return render_template('index.html')


@app.route('/receiver_quote', methods=['POST'])
def get_quote_data():
    # print("********************Im Getting the requests for processing ************************************")
    data = request.get_json()
    request_data = json.dumps(data, default=my_converter, indent=4)
    response = quote_request(request_data)
    return jsonify(response)


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
