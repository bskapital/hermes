//https://spring.io/blog/2011/04/01/routing-topologies-for-performance-and-scalability-with-rabbitmq/
//https://github.com/yitznewton/sockbit/blob/master/socketio_server.js#L42
//
var rabbitHost = process.env.DATABUS_HOST;
var rabbitPort = parseInt(process.env.DATABUS_PORT);
var rabbitUser = process.env.DATABUS_USER;
var rabbitPassword = process.env.DATABUS_PASS;
var rabbitVhost = process.env.DATABUS_VHOST;



var amqp = require('amqplib');
var when = require('when');
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);



var rabbit_conn = {
  protocol: 'amqp',
  hostname: rabbitHost,
  port: rabbitPort,
  username: rabbitUser,
  password: rabbitPassword,
  locale: 'en_US',
  frameMax: 0,
  heartbeat: 0,
  vhost: rabbitVhost,
}


// Direct exchange
var exName = 'messages-with-clients'
var qName = 'to_clients_queue'
var jobsChannel;
var leave = false;


var sendToClient = (msg) => {
  var msg_txt = JSON.parse(msg.content);
  var socket_to = msg_txt['socket_id'];
  var body = msg_txt['body'];


  io.to(socket_to).emit('from_redis', body);
}



const wait = ms => new Promise(r => setTimeout(r, ms));

// Connection handling retries: https://stackoverflow.com/a/44577075
const connect = (url, delay, times) => {
  return new Promise((resolve, reject) => {
    amqp.connect(url)
      .then(conn => resolve(conn))
    .catch(err => {
      console.log('Unable to connect to RabbitMQ, retrying in ' + (delay/1000) + ' seconds.')
      if (times - 1 > 0) {
        return wait(delay)
          .then(connect.bind(null, url, delay, times - 1))
          .then(resolve)
          .catch(reject);
      }
      console.log('Connection to RabbitMQ not possible, review your code!')
      return reject(err);
    })
  })
}

const createChannel = conn => {
  return new Promise((resolve, reject) => {
    conn.createChannel()
      .then(channel => resolve(channel))
      .catch(err => reject(err))
  })
}

const channelAssertExchange = (channel, exName) => {
  return new Promise((resolve, reject) => {
    channel.assertExchange(exName, 'direct', {durable: true})
      .then(asserted => resolve(channel))
      .catch(err => reject(err))
  })
}

const channelAssertQueue = (channel, qName) => {
  return new Promise((resolve, reject) => {
    channel.assertQueue(qName, {exclusive: false})
      .then(asserted => resolve(channel))
      .catch(err => reject(err))
  })
}

const bindQueueExchange = (channel, exName, qName) => {
  return new Promise((resolve, reject) => {
    channel.bindQueue(qName.queue, exName, '')
      .then(asserted => resolve(channel))
      .catch(err => reject(err))
  })

}


var sendToQueue = (socket) => {
  socket.on('frw_to_service', (message) => {

    msg = JSON.parse(message);
    var q = msg['queue'];
    var body = msg['body'];

    body.socketId = socket.id;
    if (body.toLog=='join'){
      body.clientAddress = socket.handshake.headers['x-real-ip'];
    }




    var jobString = JSON.stringify(body);
    console.log('forwarding job to ' + q + ':' + jobString);
    jobsChannel.publish(exName, q, new Buffer(jobString), {persistent:false});
  });
};


var sendDisconnect = (exName, q, socket) => {
  var body={clientAddress: socket.handshake.headers['x-real-ip'],
        socketId: socket.id,
        timestamp: Date.now(),
        toLog: 'leave'
  }
  var jobString = JSON.stringify(body);
  console.log('forwarding job to ' + q + ':' + jobString);
  jobsChannel.publish(exName, q, new Buffer(jobString), {persistent:false});
}



// Asynchronous connection setup
const connection = async (url, exName, qName) => {
  var conn = await connect(url, 5000, 10);
  var channel = await createChannel(conn);

  jobsChannel = await channel;

  var exchange = await channelAssertExchange(channel, exName);
  var assertQueue = await channelAssertQueue(channel, qName);
  var bindQueue = await bindQueueExchange(channel, exName, qName);
  return channel
}



connection(rabbit_conn, exName, qName).then((ch, q) => {
  ch.consume(qName.queue, (msg) => {
    console.log('Receiving announcement from service: ' + msg.content);
    sendToClient(msg);
  }, {noAck: false});


}).then(function(){
   console.log('Listening for announcements from clients');
   io.on('connection', (socket) => {
     console.log('User connected at socket: ' + socket.id);
     sendToQueue(socket);

     socket.on('disconnect', (reason) => {
       console.log('User disconnect ' + reason);
       sendDisconnect(exName, 'status-update', socket);

     });

   });

})




server.listen(3000, function() {
  console.log("NodeJs server up and running at http://localhost:3000");
});






//
// amqp.connect(rabbit_conn, function(err, conn) {
//   console.log('Connected at rabbit server.');
//   conn.createChannel(function(err, ch){
//     jobsChannel = ch;
//     ch.assertExchange(ex, 'direct', {durable: false});
//     ch.assertQueue(forward_to_clients_queue, {exclusive: true}, function(err, q){
//       ch.bindQueue(q.queue, ex, '');
//       console.log('Listening for announcements from services');
//       ch.consume(q.queue, function(msg) {
//         console.log('receiving announcement from service: ' + message.content);
//         console.log('Dispatching to client');
//         }, {noAck: true});
//     });
//     console.log('listening for announcements from clients');
//     io.on('connection', function(socket) {
//       console.log('a user connected at socket ' + socket.id);
//       sendToService(socket);
//     });
//   });
// });



