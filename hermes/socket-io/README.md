### NODE JS SERVER

A simple server for listening for clients locations updates in real-time and send it to Redis DB, using the GEOADD function.

also add the last update time for a client, to be used to track IDLE time and clean up the DB. AS Redis not allow to set TTL for values inside a key as the GEO is. 

Implemented using sockets.io 
