from sqlalchemy import Column, Integer, JSON, String, Date, DateTime, ForeignKey, Unicode, Float, BigInteger
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.ext import declarative
from datetime import datetime


def table_args_meta(table_args):
    """Declarative metaclass automatically adding (merging) __table_args__ to
    mapped classes.
                https://stackoverflow.com/a/25790535/3512107
                https://github.com/SmartTeleMax/iktomi/blob/master/iktomi/mysql_server/sqla/declarative.py#L66-L118
        Example:
        Meta = table_args_meta({'mysql_engine': 'InnoDB',
                              'mysql_default charset': 'utf8',
                             }
        Base = declarative_base(name='Base', metaclass=Meta)
        class MyClass(Base):
            …
    is equivalent to
        Base = declarative_base(name='Base')
        class MyClass(Base):
            __table_args__ = {
                'mysql_engine': 'InnoDB',
                'mysql_default charset': 'utf8',
            }
            …
    """
    class _TableArgsMeta(declarative.DeclarativeMeta):

        def __init__(cls, name, bases, dict_):
            if (  # Do not extend base class
                    '_decl_class_registry' not in cls.__dict__ and
                    # Missing __tablename_ or equal to None means single table
                    # inheritance — no table for it (columns go to table of
                    # base class)
                    cls.__dict__.get('__tablename__') and
                    # Abstract class — no table for it (columns go to table[s]
                    # of subclass[es]
                    not cls.__dict__.get('__abstract__', False)):
                ta = getattr(cls, '__table_args__', {})
                if isinstance(ta, dict):
                    ta = dict(table_args, **ta)
                    cls.__table_args__ = ta
                else:
                    assert isinstance(ta, tuple)
                    if ta and isinstance(ta[-1], dict):
                        tad = dict(table_args, **ta[-1])
                        ta = ta[:-1]
                    else:
                        tad = dict(table_args)
                    cls.__table_args__ = ta + (tad,)
            super(_TableArgsMeta, cls).__init__(name, bases, dict_)
    return _TableArgsMeta


Base = declarative_base(name='Base',
                        metaclass=table_args_meta({'mysql_engine': 'InnoDB'})
                        )


class Trips(Base):
    __tablename__ = "trips"
    added_id = Column('added_id', BigInteger, primary_key=True)
    row_key = Column('row_key', Unicode(36), nullable=False, unique=True)
    row_name = Column('row_name', Unicode(255), nullable=False)
    reference_key = Column('reference_key', BigInteger, nullable=False)
    body = Column('body', JSON, nullable=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class People(Base):
    __tablename__ = "people"
    added_id = Column('added_id', BigInteger, primary_key=True)
    row_key = Column('row_key', Unicode(36), nullable=False, unique=True)
    row_name = Column('row_name', Unicode(255), nullable=False)
    reference_key = Column('reference_key', BigInteger, nullable=False)
    body = Column('body', JSON, nullable=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class Vehicles(Base):
    __tablename__ = "vehicles"
    added_id = Column('added_id', BigInteger, primary_key=True)
    row_key = Column('row_key', Unicode(36), nullable=False, unique=True)
    row_name = Column('row_name', Unicode(255), nullable=False)
    reference_key = Column('reference_key', BigInteger, nullable=False)
    body = Column('body', JSON, nullable=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class Documents(Base):
    __tablename__ = "server_documents"
    added_id = Column('added_id', BigInteger, primary_key=True)
    row_key = Column('row_key', Unicode(36), nullable=False, unique=True)
    row_name = Column('row_name', Unicode(255), nullable=False)
    reference_key = Column('reference_key', BigInteger, nullable=False)
    body = Column('body', JSON, nullable=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


###################################################################################################
# Support Tables -- Mapped Classes
###################################################################################################
class VehicleMakers(Base):
    __tablename__ = "_vehicle_makers"
    id = Column('id', Integer, primary_key=True, autoincrement=False)
    maker = Column('maker', Unicode(255), nullable=False, unique=True)
    models = relationship('VehicleModels')


class VehicleModels(Base):
    __tablename__ = "_vehicle_models"
    id = Column('id', Integer, primary_key=True, autoincrement=False)
    model = Column('model', Unicode(255), nullable=False)
    vehicle_makers_id = Column('vehicle_makers_id', Integer, ForeignKey('_vehicle_makers.id'), nullable=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class VehicleYears(Base):
    __tablename__ = "_vehicle_years"
    id = Column('id', Integer, primary_key=True, nullable=False, autoincrement=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class VehiclesColors(Base):
    __tablename__ = "_vehicles_colors"
    id = Column('id', Integer, primary_key=True, autoincrement=False)
    color = Column('color', Unicode(255), nullable=False, unique=True)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class VehiclesTypes(Base):
    __tablename__ = "_vehicles_types"
    id = Column('id', Integer, primary_key=True, autoincrement=False)
    type = Column('type', Unicode(255), nullable=False, unique=True)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class PaymentMethods(Base):
    __tablename__ = "_payment_methods"
    id = Column('id', Integer, primary_key=True, autoincrement=False)
    method = Column('method', Unicode(255), nullable=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class PaymentStatus(Base):
    __tablename__ = "_payment_status"
    id = Column('id', Integer, primary_key=True, autoincrement=False)
    status = Column('status', Unicode(255), nullable=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)


class PeopleRoleStatus(Base):
    __tablename__ = "_people_role_status"
    id = Column('id', Integer, primary_key=True, autoincrement=False)
    status = Column('role', Unicode(255), nullable=False)
    created_at = Column('created_at', DateTime, nullable=False, default=datetime.utcnow)
