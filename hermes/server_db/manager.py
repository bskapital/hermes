from sqlalchemy import engine_from_config, MetaData
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.exc import SQLAlchemyError

from passlib.hash import bcrypt


class DB:
    """
    Database object that manage interactions with database
    """

    def __init__(self, database_name):
        databases_prefix = {'ff_schemaless': 'ff.'}

        self.database_name = database_name
        self.prefix = databases_prefix[database_name]
        self.config = Settings().sql_config()
        self.engine = engine_from_config(self.config, prefix=self.prefix)
        self.conn = None
        self.session = None

    def connect(self):
        try:
            self.conn = self.engine.connect()
        except (SQLAlchemyError, AttributeError):
            self.engine()
            self.conn = self.engine.connect()

    def query(self, sql):
        try:
            ans = self.conn.execute(sql)
        except (SQLAlchemyError, AttributeError):
            self.connect()
            ans = self.conn.execute(sql)
        return ans

    def table(self, name):
        return self.metadata().tables[name]

    def metadata(self):
        try:
            meta = MetaData(self.engine, reflect=True)
        except (SQLAlchemyError, AttributeError):
            self.engine()
            meta = MetaData(self.engine, reflect=True)
        return meta


def add_records(my_db, table, data):
    """
    Insert function
    :param my_db: DB object
    :param table: ORM Mapped Class
    :param data: list of dictionaries keys = columns, values= data to insert
    :return: None
    """
    Session = sessionmaker()
    Session.configure(bind=my_db.engine)
    session = Session()

    try:
        for each_dict in data:
            record = table(**each_dict)
            session.add(record)
        session.commit()
    except SQLAlchemyError as e:
        print(e)
    finally:
        session.close()



# class Client(Base):
#     __tablename__ = 'clients'
#
#     id = Column(Integer, primary_key=True)
#     firstname = Column(String(50), nullable=False)
#     surname = Column(String(50))
#     country = Column(String(50), nullable=False)
#     email = Column(String(50), nullable=False, unique=True)
#     mobile = Column(String(50), nullable=False, unique=True)
#     client_since = Column(DateTime)
#     password = Column(String(300), nullable=False)
#
#     # status = None
#     roles = Column(Integer, ForeignKey("addresses.id"))
#
#     addresses = relationship("Address", back_populates="clients")
#     vehicles = relationship("Vehicle", back_populates="clients")
#
#     def __init__(self, id, firstname, surname, country, email, mobile, password, client_since):
#         self.id = id
#         self.firstname = firstname
#         self.surname = surname
#         self.country = country
#         self.email = email
#         self.mobile = mobile
#         self.password = bcrypt.encrypt(password)
#         self.client_since = client_since
#
#
#
#     def validate_password(self, password):
#         return bcrypt.verify(password, self.password)
#
#     def __repr__(self):
#         return "<User(internal_id={0}, firstname={1}, surname={2}, country={3} " \
#                "email={4}, mobile={5}, password={6}, client since={7}>".format(self.id,
#                                                                                self.firstname,
#                                                                                self.surname,
#                                                                                self.country,
#                                                                                self.email,
#                                                                                self.mobile,
#                                                                                self.password,
#                                                                                self.client_since)
#

###################################################################################################
# Core Tables -- Mapped Classes
###################################################################################################

