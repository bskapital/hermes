"""
Populates support tables fields.
Necessary at setup or when variables change
"""

import pandas as pd
from pathlib import Path
from manager import *
from models import *
import chardet


def find_encoding(fname):
    """
    Find what encoding was used to save a file
    Thanks to: https://jrogel.com/python-3-pandas-encoding-issues/

    :param fname: file path
    :return: charset
    """
    r_file = open(fname, 'rb').read()
    result = chardet.detect(r_file)
    charenc = result['encoding']
    return charenc


def csv2dict(fname, encoding):
    """
    Reads CSV files and convert each row to Dictionary, use column names as keys
    :param fname: file path str
    :param encoding: str
    :return: list of dictionaries
    """
    data = pd.read_csv(fname, encoding=encoding, index_col=False).to_dict('records')
    return data


def populate_tables():

    db = DB('ffdatabase')
    parent = Path(__file__).parents[1]

    filename = Path(parent, 'assets', 'vehicle_makers.csv')
    my_encoding = find_encoding(filename)
    makers = csv2dict(filename, my_encoding)
    add_records(db, VehicleMakers, makers)

    filename = Path(parent, 'assets', 'vehicle_models.csv')
    my_encoding = find_encoding(filename)
    models = csv2dict(filename, my_encoding)
    add_records(db, VehicleModels, models)

    years = [{'id': 2011}, {'id': 2012}, {'id': 2013}, {'id': 2014},
             {'id': 2015}, {'id': 2016}, {'id': 2017}, {'id': 2018}, {'id': 2019}]
    add_records(db, VehicleYears, years)

    colors = [{'id': 0, 'color': 'white'}, {'id': 1, 'color': 'black'}, {'id': 2, 'color': 'red'}]
    add_records(db, VehiclesColors, colors)

    vehicle_types = [{'id': 0, 'type': 'Sedan'}, {'id': 1, 'type': 'Van'}]
    add_records(db, VehiclesTypes, vehicle_types)

    payment_methods = [{'id': 0, 'method': 'Cash'}, {'id': 1, 'method': 'Credit card'}]
    add_records(db, PaymentMethods, payment_methods)

    payment_status = [{'id': 0, 'status': 'Approved'}, {'id': 1, 'status': 'Denied'}]
    add_records(db, PaymentStatus, payment_status)

    people_roles = [{'id': 0, 'status': 'Active'}, {'id': 1, 'status': 'Suspended'},
                    {'id': 2, 'status': 'Inactive'}, {'id': 3, 'status': 'Background check'}]
    add_records(db, PeopleRoleStatus, people_roles)




