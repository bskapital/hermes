from sqlalchemy import engine_from_config
import yaml
import pathlib
from models import *
import requests


class Config:
    """General configuration options for Firefly Project."""

    def __init__(self):
        self.stream = self._stream()

    def _settings_file_path(self):
        try:
            fpath = requests.get('http://localhost:5000/z_api/v1/database/' + 'ffdatabase')
            self.config_path = pathlib.Path(fpath)
        except (KeyError, FileNotFoundError, AttributeError):
            # TODO revise exceptions here
            raise SystemError

    def _stream(self):
        self._settings_file_path()
        stream = open(self.config_path, 'rt')
        return yaml.load(stream)

    # Databases management functions
    def sql_config(self):
        return self.stream['server_mysql']


if __name__ == '__main__':
    config = Config().sql_config()
    engine = engine_from_config(config, prefix='ff.')
    Base.metadata.create_all(engine)

