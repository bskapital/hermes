var socket = io.connect('http://socket.app.test', { 'forceNew': true });



//var mapTilesServer = 'http://mapstiles:8081/styles/osm-bright/{z}/{x}/{y}.png'
var mapTilesServer = 'http://maps.app.test/styles/osm-bright/{z}/{x}/{y}.png';


var isDev = true;
var radius = 3000;
var decimalDecimals = 6;
var numberFormatCountry = 'en-US';
var clientCurrency = 'USD';
var actorUuid;

var currentLocation = L.latLng(4.66699,-74.0498737)   // Near C.C. Andino Bogota CO   To use when live location is unavailable
var endMarkerLoc = [4.6623542,-74.0634867]   // 93 Park Bogota CO
var zoomDefault = 14;
var markersPos = [];
var map;
var driverCircle;
var locations;
var actorUpdateFrequency = 2500;
var riderLocation;
var actorStatus = "off-off";    // on-looking, on-transit, on-riding, off-off, off-riding
var actorGeo = {};
var actorData={};
var driverMarker;
var geoOptions = {
      timeout: 30000,
      enableHighAccuracy: true,
      maximumAge: 0
    };




// ###############################################################################
// Main function
// ###############################################################################
//function initMap(currentLocation) {
function initMap() {

  // create map with Leaflet center at rider current location
  var mapOptions = {
    center: currentLocation,
    zoom: zoomDefault,
    dragging: true,
    trackResize: true,
  }
  map = new L.map('map', mapOptions);

  // Creating a map layer object
  var layer = new L.TileLayer(mapTilesServer);
  map.addLayer(layer);

  // START MARKER
  var driverIcon = new L.icon({
    iconUrl: '../static/assets/car.png',
    iconSize: [25, 25],
  });
  driverMarker = new L.marker(currentLocation, {
    icon: driverIcon,
    draggable: true,
    alt: 'Start'
  }).addTo(map);

  actorUuid=uuid4();


  actorGeo.actorType='driver';
  actorGeo.job='location-update';
  actorGeo.actorUuid=actorUuid;
  actorGeo.lat=driverMarker.getLatLng().lat;
  actorGeo.lng=driverMarker.getLatLng().lng;


  actorData.actorUuid=actorUuid;
  actorData.actorType='driver';
  actorData.job='status-update';
  actorData.deviceStatus='on';;
  actorData.driverStatus='off'
  actorData.tripStatus='off';


  // Logging initial connection
  myEmit('connection-update');
  myEmit('status-update');



  // Fit zoom to bounds
  markersPos = [driverMarker.getLatLng()];
  map.panTo(driverMarker.getLatLng())

  // Update onscreen initial information
  updateMarkerPosition(driverMarker.getLatLng(), 'driver');

  // LISTENERS
  driverMarker.on('dragstart', function(e) {
    removeDriverCircle();
  });


  driverMarker.on('dragend', function(e) {
    markersPos = [driverMarker.getLatLng()];
    updateMarkerPosition(driverMarker.getLatLng(), 'driver');

    actorGeo.lat=driverMarker.getLatLng().lat;
    actorGeo.lng=driverMarker.getLatLng().lng;
    myEmit('location-update');

    map.panTo(driverMarker.getLatLng())
    if (actorStatus=='off-off') {
      removeDriverCircle();
    } else {
      driverCircleDraw(driverMarker.getLatLng());
    }
  });



  // Socket listeners
  socket.on('trips_from_server', function(data) {
      console.log("New trips:" + data);
  });





  // Update data to server every n seconds
  // Location and status data are updated.
  setInterval(function(){
    actorGeo.lat=driverMarker.getLatLng().lat;
    actorGeo.lng=driverMarker.getLatLng().lng;
    actorGeo.timestamp=Date.now();
    myEmit('location-update');
      }, actorUpdateFrequency);



}





// ###############################################################################
// Manage trips table
// ###############################################################################

function addRow(tripData) {
  // Get a reference to the table
  var tableRef = document.getElementById("tripsTable");

  // Insert a row in the table at row index 0
  var newRow = tableRef.insertRow(0);

  // Insert new cells (<td> elements)
  // time
  var cell0 = row.insertCell(0);
  // distance
  var cell1 = row.insertCell(1);
  // fare
  var cell2 = row.insertCell(2);
  // surge
  var cell3 = row.insertCell(3);

  // Add some text to the new cells:
  cell0.innerHTML = tripData.time;
  cell1.innerHTML = tripData.distance;
  cell2.innerHTML = tripData.fare;
  cell3.innerHTML = tripData.surge;
  cell4.innerHTML = tripData.tripUuid
}



function deleteRow(tripUuid){
  // Get a reference to the table
  var tableRef = document.getElementById("tripsTable");

  tableRef.deleteRow(1);
  // delete the second row
}


function deleteAllRows(){

    var tableHeaderRowCount = 1;
    var table = document.getElementById('tripsTable');
    var rowCount = table.rows.length;
    for (var i = tableHeaderRowCount; i < rowCount; i++) {
        table.deleteRow(tableHeaderRowCount);
}
}


// https://www.w3schools.com/howto/howto_js_sort_table.asp
function sortTable(){
  console.log("To implement en app. NOt wasting time for web mock in development.");
  return false;
}




// ############################################################################
// Status Updates Switches
// ############################################################################
//https://stackoverflow.com/a/37294466



// Status Switch using Socket.Io
(function() {
  $(document).ready(function() {
    $('#statusSwitch').on('change', function() {
      var isChecked = $(this).is(':checked');

      if(isChecked) {
        actorData.connectionStatus='on';
        actorData.driverStatus='on';
        actorData.tripStatus='looking';
        driverCircleDraw(driverMarker.getLatLng());
      } else {
        actorData.connectionStatus='off';
        actorData.driverStatus='off';
        removeDriverCircle();
      }

      myEmit('status-update');

    });
  });
})();



//Automatic mode
(function() {
  $(document).ready(function() {
    $('#autoSwitch').on('change', function() {
      var isChecked = $(this).is(':checked');

      if(isChecked) {
        console.log("I'm in auto mode")
      } else {
        console.log("I'm in manual mode")
      }
    });
  });

})();



// ############################################################################
// Update data set for socket transmission
// ############################################################################

// Emit with confirmation
function myEmit(emitType) {
  if (emitType == 'connection-update') {
    queue = 'status-update';
    body = dataPack(queue, {actorUuid: actorUuid, toLog:'join', timestamp: Date.now()});

  } else if (emitType == 'status-update') {
    queue = 'status-update';
    actorData.timestamp=Date.now();
    body = dataPack(queue, actorData);

  } else if (emitType == 'location-update'){
    queue = 'status-update';
    actorGeo.timestamp=Date.now();
    body = dataPack(queue, actorGeo);
  };


  socket.emit('frw_to_service', body, function(response) {
    console.log(response);
  });

}



// prepare the data with info to be used for constructing a RabbitMQ message
function dataPack(queue, body) {
  x ={
    queue: queue,
    body: body
    };
  return JSON.stringify(x)
}


function updateLocations(startMarkerPos, endMarkerPos) {
  locations = {
    "start": {
      "lat": startMarkerPos.lat,
      "lng": startMarkerPos.lng
    },
    "end": {
      "lat": endMarkerPos.lat,
      "lng": endMarkerPos.lng
    }
  };
}


// ############################################################################
// Draw figures
// ############################################################################
function removeDriverCircle() {
  if (driverCircle != undefined) {
    map.removeLayer(driverCircle);
  };
}

function driverCircleDraw(pos) {
  driverCircle = L.circle(pos, radius, {
    fillColor: '#546B9F',
    fillOpacity: 0.4,
    stroke: true,
    color: '#A9AEEA',
    weight: 3,
  }).addTo(map);
}


// ############################################################################
// Get current position from device
//https://www.tutorialspoint.com/html5/geolocation_getcurrentposition.htm
// ############################################################################
function returnLocation(position) {
  var loc = L.latLng(position.coords.latitude, position.coords.longitude);
  initMap(loc);
}

function errorHandler(err) {
  if (err.code == 1) {
    alert("Error: Access is denied!");
  } else if (err.code == 2) {
    alert("Error: Position is unavailable!");
  }
}

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(returnLocation, errorHandler, geoOptions);
  } else {
    alert("Sorry, browser does not support geolocation!");
  }
}


// ##########################################################################
// changes the position of the marker
// ##########################################################################

function updateMarkerPosition(latLng, loc) {
  if (loc == 'start') {
    document.getElementById('startLat').innerHTML = formatDecimal(latLng.lat, decimalDecimals);
    document.getElementById('startLng').innerHTML = formatDecimal(latLng.lng, decimalDecimals);
  } else if (loc == 'end') {
    document.getElementById('endLat').innerHTML = formatDecimal(latLng.lat, decimalDecimals);
    document.getElementById('endLng').innerHTML = formatDecimal(latLng.lng, decimalDecimals);
  } else if (loc == 'driver') {
    document.getElementById('driverLat').innerHTML =formatDecimal(latLng.lat, decimalDecimals);
    document.getElementById('driverLng').innerHTML =  formatDecimal(latLng.lng, decimalDecimals);
  }
}

// ##########################################################################
// Formatting
// ##########################################################################
function formatDecimal(num, dec){
    //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed
    return parseFloat(num).toFixed(dec);
}

function formatCurrency(num, dec){
    // Create our number formatter.   https://stackoverflow.com/a/16233919
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat
    var formatter = new Intl.NumberFormat(numberFormatCountry, {
      style: 'currency',
      currency: clientCurrency,
    });
    return formatter.format(num, 0)
}

function fitBounds(){
  // Fit to Bounds
  var bounds = new L.LatLngBounds(markersPos);
  map.fitBounds(bounds, {
     padding: [50, 50]
   });


}

// ##########################################################################
// Some functions
// ##########################################################################
//This is for development only. In real life the UUID must be creater at the
// server and be unique.
//https://stackoverflow.com/a/2117523
function uuid4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )
}


//Create random lat/long coordinates in a specified radius around a center point
// https://stackoverflow.com/a/31280435
function randomGeo(center, radius) {
    var y0 = center.lat;
    var x0 = center.lng;
    var rd = radius / 111300; //about 111300 meters in one degree

    var u = Math.random();
    var v = Math.random();

    var w = rd * Math.sqrt(u);
    var t = 2 * Math.PI * v;
    var x = w * Math.cos(t);
    var y = w * Math.sin(t);

    //Adjust the x-coordinate for the shrinking of the east-west distances
    var xp = x / Math.cos(y0);

    var newlat = y + y0;
    var newlon = x + x0;
    var newlon2 = xp + x0;

    return {
        'lat': newlat.toFixed(5),
        'lng_no_adj': newlon.toFixed(5),
        'lng': newlon2.toFixed(5),
    };
}
