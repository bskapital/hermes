"""
Mock of a client_rider_desktop

All this must be implemented for IOs and Android

"""
import json
from datetime import datetime
import pika
import uuid
import os

DATABUS_HOST = os.environ.get('DATABUS_HOST')
DATABUS_PORT = os.environ.get('DATABUS_PORT')
DATABUS_VHOST = os.environ.get('DATABUS_VHOST')
DATABUS_USER = os.environ.get('DATABUS_USER')
DATABUS_PASS = os.environ.get('DATABUS_PASS')


# RPC rabbit asking for quote - Rider
def quote_request(data):
    pricing_rpc = RpcClient(queue='pricing_queue')
    my_request = pricing_rpc.call(data)
    response = json.loads(my_request)
    return response


# Post update different to geo data. GeoData is send with socket.io
def post_update(data):
    """ Send message to server routing to ex: incoming form clients and routing key status_updates.
        At the server there must be a service listening

    Based on: https://www.rabbitmq.com/tutorials/tutorial-four-python.html

    :param data: json
    :return: ack
    """
    credentials = pika.PlainCredentials(DATABUS_USER, DATABUS_PASS)
    parameters = pika.ConnectionParameters(DATABUS_HOST, DATABUS_PORT, DATABUS_VHOST, credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    # Turn on delivery confirmations
    channel.confirm_delivery()

    channel.exchange_declare(exchange='incoming_from_clients',
                             exchange_type='direct')

    if channel.basic_publish(exchange='incoming_from_clients',
                             routing_key='status_updates',
                             body=data,
                             properties=pika.BasicProperties(content_type='application/json',
                                                             delivery_mode=2)
                             ):
        print(" [x] Sent {}: {}".format('status_updates: ', data))
    else:
        print('Message could not be confirmed')
    connection.close()
    return True


class RpcClient(object):
    """ Remote Procedure Call thought RabbitMQ with direct reply-to

    Based on: https://www.rabbitmq.com/tutorials/tutorial-six-python.html
    Modify because: https://www.rabbitmq.com/direct-reply-to.html

    RPC (request/reply) is a popular pattern to implement with a messaging broker like RabbitMQ.
    The typical way to do this is for RPC clients to send requests that are routed to a long lived (known)
    server queue. The RPC server(s) consume requests from this queue and then send replies to each client
    using the queue named by the client in the reply-to header.

    Where does the client's queue come from? The client can declare a single-use queue for each
    request-response pair. But this is inefficient; even a transient unmirrored queue can be expensive
    to create and then delete (compared with the cost of sending a message). This is especially true
    in a cluster as all cluster nodes need to agree that the queue has been created, even if it is unmirrored.

    So alternatively the client can create a long-lived queue for its replies. But this can be fiddly to manage,
    especially if the client itself is not long-lived.

    The direct reply-to feature allows RPC clients to receive replies directly from their RPC server,
    without going through a reply queue. ("Directly" here still means going through the same connection
    and a RabbitMQ node; there is no direct network connection between RPC client and RPC server processes.)

    ...

    Reply messages sent using this mechanism are in general not fault-tolerant; they will be discarded
    if the client that published the original request subsequently disconnects.
     The assumption is that an RPC client will reconnect and submit another request in this case.
    """

    def __init__(self, queue):

        self.credentials = pika.PlainCredentials(DATABUS_USER, DATABUS_PASS)
        self.parameters = pika.ConnectionParameters(DATABUS_HOST, DATABUS_PORT, DATABUS_VHOST, self.credentials)
        self.connection = pika.BlockingConnection(self.parameters)
        self.queue = queue
        self.channel = self.connection.channel()

        self.channel.basic_consume(self.on_response,
                                   no_ack=True,
                                   queue='amq.rabbitmq.reply-to')

        self.response = None
        self.corr_id = None

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, n):
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='',
                                   routing_key=self.queue,
                                   properties=pika.BasicProperties(reply_to='amq.rabbitmq.reply-to',
                                                                   correlation_id=self.corr_id,
                                                                   content_type='application/json'),
                                   body=n)
        while self.response is None:
            self.connection.process_data_events()
        return self.response


def my_converter(o):
    """
    serialize  datetime

    It is easy to serialize a Python data structure as JSON, we just need to call
    the json.dumps method, but if our data stucture contains a datetime object
    we'll get an exception: TypeError: datetime.datetime(...) is not JSON serializable

    The json.dumps method can accept an optional parameter called default which is
    expected to be a function. Every time JSON tries to convert a value it does not
    know how to convert it will call the function we passed to it. The function
    will receive the object in question, and it is expected to return the JSON
    representation of the object.

    In the function we just call the __str__ method of the datetime object that
    will return a string representation of the value. This is what we return.

    https://code-maven.com/serialize-datetime-object-as-json-in-python

    :param o:
    :return: str
    """
    if isinstance(o, datetime):
        return o.__str__()
