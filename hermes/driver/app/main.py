from flask import Flask, render_template, request, jsonify
from functions import quote_request, my_converter, post_update
import json


app = Flask(__name__)


@app.route("/")
def main():
    return render_template('index.html')


@app.route('/receiver_quote', methods=['POST'])
def get_quote_data():
    data = request.get_json()
    request_data = json.dumps(data, default=my_converter, indent=4)
    response = quote_request(request_data)
    return jsonify(response)


@app.route('/send_update', methods=['POST'])
def send_update():
    data = request.get_json()
    request_data = json.dumps(data, default=my_converter, indent=4)
    response = post_update(request_data)
    return jsonify(response)


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
