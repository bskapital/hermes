#!/bin/bash

dir=$1
rootPath="$PWD"

nginxVersion=1.13  # Could also be passed as an argument using $2





result=$(docker run --rm -t -a stdout --name my-nginx -v ${rootPath}/${dir}/:/etc/nginx/:ro nginx:$nginxVersion nginx -c /etc/nginx/nginx.conf -t)

# Thanks to:
#https://dev.to/simdrouin/validate-your-nginx-configuration-files-easily-with-docker-4ihi


# Look for the word successful and count the lines that have it
# This validation could be improved if needed
successful=$(echo $result | grep successful | wc -l)

if [ $successful = 0 ]; then
    echo FAILED
    echo "$result"
    exit 1
else
    echo SUCCESS
fi